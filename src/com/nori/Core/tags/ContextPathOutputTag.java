package com.nori.Core.tags;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class ContextPathOutputTag extends TagSupport {

	private static final long serialVersionUID = 1L;

	@Override
	public int doStartTag() throws JspException {
		try {
			HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
			pageContext.getOut().print(request.getContextPath() == null ? "" : request.getContextPath());
		} catch (IOException ex) {
		}
		return SKIP_BODY;
	}
}
