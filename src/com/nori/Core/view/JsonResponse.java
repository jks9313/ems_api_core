package com.nori.Core.view;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value=Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonResponse {

	private boolean success;
	private String message;
	private Long total;
	private Long no;
	private Long size;
	@JsonIgnore
	private HttpStatus status;
	private Map<String, String> errors;
	private Map<String, Object> map;
	private List<? extends Object> rows;
	private List<? extends Object> footer;

	public JsonResponse() {
		this.success = true;
		this.status = HttpStatus.OK;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public void setTotal(Integer total) {
		this.total = total.longValue();
	}

	public Long getNo() {
		return no;
	}

	public void setNo(Long no) {
		this.no = no;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public Map<String, String> getErrors() {
		return errors;
	}

	public void setErrors(Map<String, String> errors) {
		this.errors = errors;
	}

	public Map<String, Object> getMap() {
		return map;
	}

	public void setMap(Map<String, Object> map) {
		this.map = map;
	}

	public List<? extends Object> getRows() {
		return rows;
	}

	public void setRows(List<? extends Object> rows) {
		this.rows = rows;
	}

	public List<? extends Object> getFooter() {
		return footer;
	}

	public void setFooter(List<? extends Object> footer) {
		this.footer = footer;
	}

	public void put(String name, Object value) {
		if (this.map == null) {
			this.map = new HashMap<String, Object>();
		}
		this.map.put(name, value);
	}

	public Object get(String name) {
		if (this.map == null) {
			this.map = new HashMap<String, Object>();
		}
		return this.map.get(name);
	}

	public void addBindingResult(BindingResult bindingResult) {
		if (bindingResult != null && bindingResult.hasErrors()) {
			createErrorsMap();
			for (ObjectError error : bindingResult.getAllErrors()) {
				String key = (error instanceof FieldError ? ((FieldError) error).getField() : error.getObjectName());
				errors.put(key, error.getDefaultMessage());
			}
		}
	}

	public void setBindingResult(BindingResult bindingResult) {
		removeErrorsMap();
		if (bindingResult != null && bindingResult.hasErrors()) {
			createErrorsMap();
			for (ObjectError error : bindingResult.getAllErrors()) {
				String key = (error instanceof FieldError ? ((FieldError) error).getField() : error.getObjectName());
				errors.put(key, error.getDefaultMessage());
			}
		}
	}

	public void clearErrors() {
		if (errors != null) {
			errors.clear();
		}
		errors = null;
	}

	private void createErrorsMap() {
		if (errors == null) {
			errors = new HashMap<String, String>();
		}
	}

	private void removeErrorsMap() {
		if (errors != null) {
			errors.clear();
		}
		errors = null;
	}

	public Long getPageNo() {
		return no;
	}

	public void setPageNo(long pageNo) {
		this.no = pageNo;
	}

	public Long getPageSize() {
		return size;
	}

	public void setPageSize(long pageSize) {
		this.size = pageSize;
	}
}
