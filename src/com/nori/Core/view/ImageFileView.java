package com.nori.Core.view;

import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.view.AbstractView;

public class ImageFileView extends AbstractView {
	
	private static Logger logger = Logger.getLogger(ImageFileView.class);

	@Override
	protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		DownloadFile downloadFile = (DownloadFile) model.get("imageFile");

		FileInputStream fin = null;
		OutputStream fout = null;
		try {
			response.reset();
			response.setContentType(downloadFile.getFileMime() + "; charset=UTF-8");
			response.setHeader("Content-Length", "" + downloadFile.getFileSize());
			fin = new FileInputStream(downloadFile.getFile());
			byte[] readBuffer = new byte[8192]; // 8K 버퍼
			fout = response.getOutputStream();
			int numOfBytes = -1;
			while ((numOfBytes = fin.read(readBuffer)) != -1) {
				fout.write(readBuffer, 0, numOfBytes);
				fout.flush();
			}
		} catch (Exception e) {
			logger.fatal(e, e);
			throw e;
		} finally {
			try {
				if (fin != null) {
					fin.close();
				}
			} catch (Exception ex) {
			} finally {
				fin = null;
			}
			try {
				if (fout != null) {
					fout.close();
				}
			} catch (Exception ex) {
			} finally {
				fout = null;
			}
			downloadFile.dispose();
		}
	}
}
