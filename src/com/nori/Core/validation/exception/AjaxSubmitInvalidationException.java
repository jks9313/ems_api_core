package com.nori.Core.validation.exception;

import org.springframework.validation.BindingResult;

public class AjaxSubmitInvalidationException extends Exception {

	private static final long serialVersionUID = 5054736004492808781L;
	
	private BindingResult bindingResult;
	
	public AjaxSubmitInvalidationException(BindingResult bindingResult) {
		this.setBindingResult(bindingResult);
	}

	public BindingResult getBindingResult() {
		return bindingResult;
	}

	public void setBindingResult(BindingResult bindingResult) {
		this.bindingResult = bindingResult;
	}
	
}
