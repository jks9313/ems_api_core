package com.nori.Core.validation.validator;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.nori.Core.validation.constraint.KoreanPhoneNumber;
import com.nori.Core.validation.type.PhoneType;

public class KoreanPhoneNumberValidator implements ConstraintValidator<KoreanPhoneNumber, String> {

	private static final Pattern PATTERN_KOREAN_PHONE_LANDLINE = Pattern.compile("^(02|031|062|053|042|051|032|052|033|055|054|061|063|064|041|043)?([-])?(\\d\\d\\d(\\d)?)+([-])?(\\d\\d\\d\\d)+$");
	private static final Pattern PATTERN_KOREAN_PHONE_INTERNET = Pattern.compile("^(070|0303)?([-])?(\\d\\d\\d(\\d)?)+([-])?(\\d\\d\\d\\d)+$");
	private static final Pattern PATTERN_KOREAN_PHONE_MOBILE = Pattern.compile("^(010|011|016|017|018|019)?([-])?(\\d\\d\\d(\\d)?)+([-])?(\\d\\d\\d\\d)+$");
	private static final Pattern PATTERN_KOREAN_PHONE = Pattern.compile("^(02|031|062|053|042|051|032|052|033|055|054|061|063|064|041|043|070|0303|010|011|012|013|016|017|018|019)?([-])?(\\d\\d\\d(\\d)?)+([-])?(\\d\\d\\d\\d)+$");
	private PhoneType[] phoneTypes;

	@Override
	public void initialize(KoreanPhoneNumber annotation) {
		phoneTypes = annotation.type();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null || "".equals(value)) {
			return true;
		}

		for (PhoneType phoneType : phoneTypes) {
			if (phoneType.equals(PhoneType.LANDLINE) && PATTERN_KOREAN_PHONE_LANDLINE.matcher(value).matches()) {
				return true;
			} else if (phoneType.equals(PhoneType.INTERNET) && PATTERN_KOREAN_PHONE_INTERNET.matcher(value).matches()) {
				return true;
			} else if (phoneType.equals(PhoneType.MOBILE) && PATTERN_KOREAN_PHONE_MOBILE.matcher(value).matches()) {
				return true;
			} else if (phoneType.equals(PhoneType.PHONE) && PATTERN_KOREAN_PHONE.matcher(value).matches()) {
				return true;
			}
		}

		return false;
	}
}
