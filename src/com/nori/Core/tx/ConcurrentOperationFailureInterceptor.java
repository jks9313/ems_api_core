package com.nori.Core.tx;

import java.util.Random;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

@Aspect
@Component("concurrentOperationFailureInterceptor")
public class ConcurrentOperationFailureInterceptor implements Ordered {

	private static final Logger logger = Logger.getLogger(ConcurrentOperationFailureInterceptor.class);
	private final Random random = new Random();
	private long initialDelayDefault = RetryConcurrentOperation.DEFAULT_INITIAL_DELAY;
	private long maximumDelayDefault = RetryConcurrentOperation.DEFAULT_MAXIMUM_DELAY;

	private static final int DEFAULT_MAX_RETRIES = 2;

	private int maxRetries = DEFAULT_MAX_RETRIES;
	private int order = 1;

	protected void pause(long delay) {
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

	protected long calculateDelay(int attempt, long initialDelay, long maximumDelay) {

		// Enforce sanity
		initialDelay = Math.max(initialDelay, 1);
		maximumDelay = Math.max(maximumDelay, 1);
		initialDelay = Math.min(initialDelay, maximumDelay);

		// Calculate nominal delay for this attempt, using exponential back-off; be careful to avoid overflow
		long delay = initialDelay;
		while (attempt-- > 1) {
			delay <<= 1;
			if (delay >= maximumDelay) {
				delay = maximumDelay;
				break;
			}
		}

		// Add in +/- 12.5% randomness
		final int randomRange = Math.max(1, (int) (delay >> 3));
		synchronized (this.random) {
			delay += this.random.nextInt(randomRange) - randomRange / 2;
		}

		// Enforce upper bound again
		delay = Math.min(delay, maximumDelay);

		// Done
		return delay;
	}

	@Around("@annotation(retryConcurrentOperation)")
	public Object performOperation(ProceedingJoinPoint pjp, RetryConcurrentOperation retryConcurrentOperation) throws Throwable {
		@SuppressWarnings("rawtypes")
		Class exceptionClass = retryConcurrentOperation.exception();
		int retries = retryConcurrentOperation.retries();
		if (!(retries > 0)) {
			retries = this.maxRetries;
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Attempting operation with potential for " + exceptionClass.getCanonicalName() + " with maximum " + retries + " retries");
		}

		final long initialDelay = retryConcurrentOperation.initialDelay() != -1 ? retryConcurrentOperation.initialDelay() : this.initialDelayDefault;
		final long maximumDelay = retryConcurrentOperation.maximumDelay() != -1 ? retryConcurrentOperation.maximumDelay() : this.maximumDelayDefault;

		int numAttempts = 0;
		do {
			numAttempts++;

			if (numAttempts > 1) {
				final long delay = this.calculateDelay(numAttempts, initialDelay, maximumDelay);
				if (logger.isDebugEnabled())
					logger.debug("pausing " + delay + " ms before retrying @RetryConcurrentOperation method " + pjp.getSignature());
				this.pause(delay);
				if (logger.isDebugEnabled())
					logger.debug("retrying @RetryConcurrentOperation method " + pjp.getSignature() + " (attempt " + numAttempts + ")");
			}

			try {
				return pjp.proceed();
			} catch (Throwable ex) {
				// if the exception is not what we're looking for, pass it through
				if (!exceptionClass.isInstance(ex)) {
					throw ex;
				}

				// we caught the configured exception, retry unless we've reached
				// the maximum
				if (numAttempts > retries) {
					logger.warn("Caught " + exceptionClass.getCanonicalName() + " and exceeded maximum retries (" + retries + ", rethrowing.");
					throw ex;
				}
				if (logger.isDebugEnabled()) {
					logger.debug("Caught " + exceptionClass.getCanonicalName() + " and will retry, attempts: " + numAttempts);
				}
			}
		} while (numAttempts <= retries);
		// this will never execute - we will have either succesfully returned or
		// rethrown an exception
		return null;
	}

	@Override
	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public void setMaxRetries(int maxRetries) {
		this.maxRetries = maxRetries;
	}
}
