package com.nori.Core.serializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 *
 * @author ChangHoon
 */
public class TimeSerializer extends JsonSerializer<Date> {

	@Override
	public void serialize(Date date, JsonGenerator gen, SerializerProvider provider) throws IOException, JsonProcessingException {
		
		if (date == null) {
			gen.writeNull();
		} else {
			SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
			String formattedDate = formatter.format(date);
	
			gen.writeString(formattedDate);
		}
	}
}
