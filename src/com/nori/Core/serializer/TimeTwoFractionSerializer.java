package com.nori.Core.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.util.Date;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class TimeTwoFractionSerializer extends JsonSerializer<Date> {

	private static final DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm:ss:SS");
	
	@Override
	public void serialize(Date date, JsonGenerator gen, SerializerProvider provider) throws IOException, JsonProcessingException {

		if (date == null) {
			gen.writeNull();
		} else {
			//DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm:ss:SS");
			String formattedDate = formatter.print(date.getTime());
	
			gen.writeString(formattedDate);
		}
	}
}
