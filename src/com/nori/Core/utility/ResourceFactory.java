package com.nori.Core.utility;

import java.nio.charset.Charset;
import java.util.concurrent.atomic.AtomicLong;

public class ResourceFactory {

	public AtomicLong getLong() {
		return new AtomicLong();
	}
	
	public Charset getUTF8Charset() {
		return Charset.forName("UTF-8");
	}
}
