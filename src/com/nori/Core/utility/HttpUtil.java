package com.nori.Core.utility;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @author ChangHoon
 */
public class HttpUtil {

	public static boolean isAjaxRequest(HttpServletRequest request) {
		return isAjaxRequest(request, null);
	}
	
	public static String getEncodedFileName(HttpServletRequest request, String fileName) {
		String userAgent = request.getHeader("User-Agent");
		String encodedfileName = fileName;
		try {
			if (userAgent != null && !"".equals(userAgent.trim()) && userAgent.toLowerCase().indexOf("msie") != -1) {
				encodedfileName = URLEncoder.encode(fileName, "UTF-8");
				encodedfileName = encodedfileName.replaceAll("\\+", " ");
				encodedfileName = encodedfileName.replaceAll("\\%2E", ".");
			} else {
				encodedfileName = new String(fileName.getBytes("UTF-8"), "8859_1");
			}
			return encodedfileName;
		} catch (UnsupportedEncodingException ex) {
			return fileName;
		}
	}

	public static boolean isAjaxRequest(HttpServletRequest request, HttpServletResponse response) {
		if ("XMLHttpRequest".equalsIgnoreCase(request.getHeader("X-Requested-With"))) {
			return true;
		}
//		if (response != null && response.getContentType() != null && response.getContentType().toLowerCase().startsWith("application/json")) {
//			return true;
//		}
		return false;
	}
}
