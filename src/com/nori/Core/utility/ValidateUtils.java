package com.nori.Core.utility;

import org.springframework.validation.BindingResult;

public class ValidateUtils {
	public static boolean hasErrors(BindingResult result, String ... field){
		for (int i = 0; i < field.length; i++) {
			if(result.hasFieldErrors(field[i])){
				return result.hasFieldErrors(field[i]);
			}
		}
		return false;
	}
}
