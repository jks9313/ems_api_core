package com.nori.Core.security;

import org.springframework.security.core.GrantedAuthority;

import com.nori.Core.security.SecurityGrantedAuthority;
import com.nori.Core.security.SecurityGrantedAuthority.Type;

public interface Role {

	public static final GrantedAuthority SUPER_ADMIN = new SecurityGrantedAuthority("ROLE_SUPER_ADMIN", "슈퍼 관리자",Type.SUPER_ADMIN);
	public static final GrantedAuthority ADMIN = new SecurityGrantedAuthority("ROLE_ADMIN", "관리자",Type.ADMIN);
	public static final GrantedAuthority USER = new SecurityGrantedAuthority("ROLE_USER", "사용자",Type.USER);
	
}
