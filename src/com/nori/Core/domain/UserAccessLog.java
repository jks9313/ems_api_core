package com.nori.Core.domain;

import java.util.Date;

import org.apache.ibatis.type.Alias;

@Alias("userAccessLog")
public class UserAccessLog {

	private Date accessDate;

	private long accessNo;

	private Date accessTime;

	private String accessIp;

	private String userId;

	private String logoutDate;

	private String logoutTime;

	private String logoutType;

	private String userAgent;

	private String referenceUri;

	private String regDate;

	public Date getAccessDate() {
		return accessDate;
	}

	public void setAccessDate(Date accessDate) {
		this.accessDate = accessDate;
	}

	public long getAccessNo() {
		return accessNo;
	}

	public void setAccessNo(long accessNo) {
		this.accessNo = accessNo;
	}

	public Date getAccessTime() {
		return accessTime;
	}

	public void setAccessTime(Date accessTime) {
		this.accessTime = accessTime;
	}

	public String getAccessIp() {
		return accessIp;
	}

	public void setAccessIp(String accessIp) {
		this.accessIp = accessIp;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getLogoutDate() {
		return logoutDate;
	}

	public void setLogoutDate(String logoutDate) {
		this.logoutDate = logoutDate;
	}

	public String getLogoutTime() {
		return logoutTime;
	}

	public void setLogoutTime(String logoutTime) {
		this.logoutTime = logoutTime;
	}

	public String getLogoutType() {
		return logoutType;
	}

	public void setLogoutType(String logoutType) {
		this.logoutType = logoutType;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getReferenceUri() {
		return referenceUri;
	}

	public void setReferenceUri(String referenceUri) {
		this.referenceUri = referenceUri;
	}

	public String getRegDate() {
		return regDate;
	}

	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}

	

}
